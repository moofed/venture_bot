require 'rails_helper'

RSpec.describe Source, type: :model do
  describe ".outdated" do
    subject { described_class.outdated }
    before do
      @source_one = described_class.create name: 'Test One', uri: 'http://test1',
                                           frequency: 5.minutes, last_refreshed: Time.now - 4.minutes
      @source_two = described_class.create name: 'Test Two', uri: 'http://test2',
                                           frequency: 5.minutes, last_refreshed: Time.now - 6.minutes
    end

    it "returns only sources that have not been refreshed recently" do
      expect(subject).to contain_exactly @source_two
    end
  end

  describe '#refresh' do
    subject { source.refresh }
    let(:uri) {"http://localhost/test/warhosrn.ics"}
    let(:source) {described_class.create name: 'Test One', uri: uri}

    it "creates events contained in the .ics file" do
      stub_request(:any, uri).with(headers: {'User-Agent' => 'VentureBot'}).to_return(body: open("#{fixture_path}/warhorn.ics").read)
      expect(Activity).to receive(:import_event).with(any_args).exactly(9).times
      expect(subject).to all(be_an Activity)
    end
  end
end

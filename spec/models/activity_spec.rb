require 'rails_helper'

RSpec.describe Activity, type: :model do
  let(:ics) { Icalendar::Calendar.parse File.open(fixture_path + '/warhorn.ics') }
  let(:event) { ics.first.events.first }
  let(:source) { Source.create! name: 'Test', uri: 'http://test' }

  describe '#import_event' do
    subject { described_class.import_event(event, source) }

    it 'creates an object with correct data' do
      expect(subject).to be_an Activity
      expect(subject).to be_valid
      expect(subject.uid).to eq 'https://warhorn.net/events/cr-pfs/schedule/2019/09/23#339860'
      expect(subject.dtstamp).to eq Time.parse('2019-09-18 14:10:21Z')
      expect(subject.dtstart).to eq Time.parse('2019-09-23 22:00:00Z')
      expect(subject.dtend).to eq Time.parse('2019-09-24 02:00:00Z')
      expect(subject.summary).to eq 'Pathfinder & Starfinder Society at The Common Room: PFS1 7-21: The Sun Orchid Scheme'
      expect(subject.description).to start_with "Pathfinder 1st Edition, Pathfinder Society (1st edition), Sovereign Court\n\nCharacter levels 1-5"
      expect(subject.source).to eq source
    end
  end

  describe '.grok' do
    let(:activity) { described_class.import_event(event, source) }
    subject { activity.grok }

    it 'caches the results' do
      expect(activity).to receive(:grok).twice.and_call_original
      expect(activity).to receive(:populate_info).once.and_call_original
      subject
      activity.grok
    end

    it 'returns a kind of HashWithIndifferentAccess' do
      expect(subject).to be_a_kind_of HashWithIndifferentAccess
    end

    it 'has tags' do
      expect(subject[:tags]).to contain_exactly('Pathfinder 1st Edition', 'Sovereign Court', 'Pathfinder Society (1st edition)')
    end

    it 'has tiers' do
      expect(subject[:tiers]).to eq('Character levels 1-5')
    end

    it 'has an author' do
      expect(subject[:author]).to eq('Written by Nicholas Wasko')
    end

    it 'has a description' do
      expect(subject[:description]).to eq('With the power to vastly extend life, the sun orchid elixir is one of the ' +
        'most prized items in the Inner Sea—and as a result one of the most dangerous to transport. After his shipments ' +
        'of sun orchid elixir vanished without a trace two years in a row, the ruler of Pashow is desperate to ensure ' +
        'that his next delivery goes off without a hitch. In order to test its security, Pashow has hired a team of ' +
        'Pathfinders to execute their finest heist and test the convoy’s defenses. Are the PCs up to the challenge?')
    end

    it 'has notes' do
      expect(subject[:notes]).to eq('Content in “The Sun Orchid Scheme” also contributes directly to the ongoing storyline of the Sovereign Court faction.')
    end

    it 'has a summary' do
      expect(subject[:summary]).to eq('Pathfinder & Starfinder Society at The Common Room: PFS1 7-21: The Sun Orchid Scheme')
    end

    it 'has a location' do
      expect(subject[:location]).to eq('Common Room Games, 223 S. Pete Ellis Dr, Bloomington, IN, US 47408')
    end

    it 'has a start time in localtime and rfc2822' do
      expect(subject[:start]).to eq('Mon, 23 Sep 2019 18:00:00 -0400')
    end

    it 'has an end time in localtime and rfc2822' do
      expect(subject[:end]).to eq('Mon, 23 Sep 2019 22:00:00 -0400')
    end

    it 'has a url' do
      expect(subject[:url]).to eq('https://warhorn.net/events/cr-pfs/schedule/2019/09/23#339860')
    end
  end
end

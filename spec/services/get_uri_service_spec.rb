require 'rails_helper'

RSpec.describe VentureBot::GetUriService do
  let(:opts) {{}}
  let(:now) {Time.now.httpdate}

  subject { described_class.new(uri, opts).call }

  context 'when uri scheme is http://' do
    let(:uri) {URI("http://localhost/test/warhorn.ics")}

    context 'with a successful response' do
      it 'succeeds and has metadata' do
        stub_request(:any, 'http://localhost/test/warhorn.ics').with(headers: {'User-Agent' => 'VentureBot'}).to_return(body: open("#{fixture_path}/warhorn.ics").read,
                                                                          headers: {last_modified: now})
        expect(subject).to eq open("#{fixture_path}/warhorn.ics").read
        expect(subject.status[0]).to eq '200'
        expect(subject.last_modified).to eq now
      end
    end

    context 'with a failed response' do
      it 'has metadata indicating the error' do
        stub_request(:any, 'http://localhost/test/warhorn.ics').to_return(body: 'Not found', status: 404)
        expect{subject}.to raise_error OpenURI::HTTPError
      end
    end

    context 'with a timed out response' do
      it 'has metadata indicating failure' do
        stub_request(:any, 'http://localhost/test/warhorn.ics').to_timeout
        expect{subject}.to raise_error Timeout::Error
      end
    end
  end

  context 'when uri scheme is something else' do
    let(:uri) {URI("nonsense://badrequest")}

    it 'fails' do
      expect { subject }.to raise_error ArgumentError
    end
  end

  context 'when uri is not a uri' do
    let(:uri) {'Not a uri'}

    it 'fails' do
      expect { subject }.to raise_error TypeError
    end
  end
end

class Activity < ApplicationRecord
  belongs_to :source

  validates :uid, :dtstamp, :dtstart, presence: true

  default_scope { newest }
  scope :newest, -> { order :dtstart, :desc }
  scope :future, -> { where "dtstart > ?", Time.now }

  def self.import_event(event, src)
    raise ArgumentError unless event.is_a? Icalendar::Event

    prop_names = event.class.properties
    prop_hash = prop_names.map {|n| [n, event.send(n)]}.to_h
    prop_hash.delete_if {|k, v| v.blank? }
    prop_hash[:source] = src

    act = create! prop_hash
    Rails.logger.info "Imported #{act.uid}"
    act
  end

  def grok
    return @info ||= populate_info
  end

private

  def populate_info
    info = HashWithIndifferentAccess.new

    lines = description.lines.map do |l|
      ls = l.strip
      ls.empty? ? nil : ls
    end
    lines.compact!
    # puts lines.pretty_inspect

    info[:tags] = lines[0].split(',').map do |t|
      t.strip
    end
    info[:tiers] = lines[1]
    info[:author] = lines[2]
    info[:description] = lines[3]
    info[:notes] = lines[4]
    info[:summary] = summary
    info[:location] = location
    info[:start] = dtstart.localtime.rfc2822
    info[:end] = dtend.localtime.rfc2822
    info[:url] = url

    return info
  end
end

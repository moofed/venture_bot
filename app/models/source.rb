class Source < ApplicationRecord
  has_many :activities

  validates :name, :uri, presence: true

  scope :active, -> { where active: true }
  scope :outdated, -> { where "strftime('%s', 'now') - strftime('%s', last_refreshed) > frequency" }

  def refresh
    response = VentureBot::GetUriService.new(URI(uri)).call
    self.last_refreshed = Time.now
    Rails.logger.info "Refreshing #{response.base_uri} #{response.status.join}"
    if response.status[0].to_s != '200'
      save
      return false
    end

    data = response
    self.last_modified = response.last_modified
    ical = Icalendar::Calendar.parse data
    events = ical.first.events

    events.collect do |e|
      a = Activity.find_by(uid: e.uid.to_s) or Activity.import_event(e, self)
      a
    end
  end
end

require 'open-uri'

module VentureBot
  class GetUriService
    def initialize(uri, opts = {})
      raise TypeError, 'URI required' unless uri.is_a? URI
      @uri = uri
      @opts = opts.merge('User-Agent' => 'VentureBot')
    end

    def call
      case @uri.scheme
      when 'http', 'https'
        @uri.read(@opts)
      else
        raise ArgumentError, 'Unsupported URI scheme'
      end
    end
  end
end

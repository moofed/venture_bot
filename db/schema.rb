# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_09_18_183420) do

  create_table "activities", force: :cascade do |t|
    t.string "uid"
    t.datetime "dtstamp"
    t.datetime "dtstart"
    t.datetime "dtend"
    t.text "description"
    t.text "location"
    t.text "summary"
    t.string "url"
    t.integer "source_id"
    t.datetime "last_announced"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["dtstart"], name: "index_activities_on_dtstart"
    t.index ["last_announced"], name: "index_activities_on_last_announced"
    t.index ["source_id"], name: "index_activities_on_source_id"
  end

  create_table "sources", force: :cascade do |t|
    t.string "name"
    t.string "uri"
    t.boolean "active"
    t.integer "frequency"
    t.datetime "last_refreshed"
    t.datetime "last_modified"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.binary "data"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end

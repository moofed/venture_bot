class CreateActivities < ActiveRecord::Migration[6.0]
  def change
    create_table :activities do |t|
      t.string :uid, unique: true
      t.datetime :dtstamp
      t.datetime :dtstart
      t.datetime :dtend
      t.text :description
      t.text :location
      t.text :summary
      t.string :url
      t.belongs_to :source
      t.datetime :last_announced
      t.timestamps
      t.index :dtstart
      t.index :last_announced
    end
  end
end

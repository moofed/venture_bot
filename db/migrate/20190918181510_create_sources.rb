class CreateSources < ActiveRecord::Migration[6.0]
  def change
    create_table :sources do |t|
      t.string :name
      t.string :uri
      t.boolean :active
      t.integer :frequency
      t.datetime :last_refreshed
      t.datetime :last_modified
      t.timestamps
      t.binary :data
    end
  end
end

require 'rufus-scheduler'

sched = Rufus::Scheduler.singleton frequency: '1m', lockfile: 'tmp/rufus-scheduler.lock'

# unless defined?(Rails::Console) || Rails.env.test? || File.split($0).last == 'rake'
#   sched.every '1h' do
#     VentureBot::AnnouncementService.run
#   end
# end
